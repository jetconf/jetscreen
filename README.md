# Jetscreen

Universal RESTCONF client.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

:warning: Works only with the [JetConf](https://github.com/CZ-NIC/jetconf) implementation.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
