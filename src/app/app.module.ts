import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { DescriptionComponent } from './description/description.component';
import { ContainerNodeComponent } from './container-node/container-node.component';
import { InternalMembersComponent } from './internal-members/internal-members.component';
import { ListNodeComponent } from './list-node/list-node.component';
import { ScalarValueComponent } from './scalar-value/scalar-value.component';
import { TerminalMembersComponent } from './terminal-members/terminal-members.component';
import { RestconfService } from './restconf.service';
import { requestOptionsProvider } from './default-request-options.service';
import { ListEntriesComponent } from './list-entries/list-entries.component';

@NgModule({
  declarations: [
    AppComponent,
    DescriptionComponent,
    ContainerNodeComponent,
    InternalMembersComponent,
    ListNodeComponent,
    ScalarValueComponent,
    TerminalMembersComponent,
    ListEntriesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule
  ],
  providers: [
    RestconfService,
    requestOptionsProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
