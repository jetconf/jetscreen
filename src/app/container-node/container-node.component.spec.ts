import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerNodeComponent } from './container-node.component';

describe('ContainerNodeComponent', () => {
  let component: ContainerNodeComponent;
  let fixture: ComponentFixture<ContainerNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
