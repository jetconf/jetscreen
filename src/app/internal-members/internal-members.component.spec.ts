import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalMembersComponent } from './internal-members.component';

describe('InternalMembersComponent', () => {
  let component: InternalMembersComponent;
  let fixture: ComponentFixture<InternalMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
