import { Component, Input } from '@angular/core';

import { TreeNode } from '../tree-node';

@Component({
  selector: 'app-internal-members',
  templateUrl: './internal-members.component.html'
})
export class InternalMembersComponent {
  @Input()
  subtrees: TreeNode[];
}
