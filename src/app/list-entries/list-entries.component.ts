import {Component, Input} from '@angular/core';
import {ListNode} from '../tree-node';

@Component({
  selector: 'app-list-entries',
  templateUrl: './list-entries.component.html',
  styleUrls: ['./list-entries.component.scss']
})
export class ListEntriesComponent {
  @Input()
  node: ListNode;
}



