import { Component, Input, OnInit } from '@angular/core';

import { ListNode } from '../tree-node';
import { ObjectValue, TerminalObject } from '../instance-data';
import { RestconfService } from '../restconf.service';

@Component({
  selector: 'app-list-node',
  templateUrl: './list-node.component.html',
  styleUrls: ['./list-node.component.scss']
})
export class ListNodeComponent implements OnInit {
  @Input()
  node: ListNode;

  constructor(private restconfService: RestconfService) {}

  handleExpand(): void {
    this.node.toggleExpand();
    if (this.node.expanded && !this.node.populated) {
      this.loadData();
    }
  }

  handleSelect(): void {
    this.node.select();
    if (this.node.selected && !this.node.populated) {
      this.loadData();
    }
  }

  switchEntry(i: number) {
    this.node.switchEntry(i);
    if (i > 0 && (this.node.expanded || this.node.selected)) {
      this.loadData();
    }
  }

  private loadData(): void {
    if (this.node.padre && this.node.padre.preloaded) {
      this.loadAll(this.node.padre.data[this.node.name]);
    } else if (this.node.digest.keys.length > 0) {
      this.restconfService.getAllKeys(this.node.path(), this.node.digest.keys)
        .subscribe(keys => this.loadKeys(keys),
                   error => console.log(error));
    } else {
      this.restconfService.getContent(this.node.resourceID())
        .subscribe(ary => this.loadAll(<ObjectValue[]>ary),
                   error => console.log(error));
    }
  }

  private loadKeys(keys: TerminalObject[]): void {
    this.node.data = keys;
    this.restconfService.getContent(this.node.resourceID(), 2)
      .subscribe(en => this.node.populate(en[0]),
        error => console.log(error));
  }

  private loadAll(ary: ObjectValue[]): void {
    this.node.data = ary;
    this.node.preloaded = true;
    this.node.populate(ary[this.node.activeIndex]);
  }

  ngOnInit(): void {
    this.restconfService.getLength(this.node.path())
      .subscribe(len => this.node.length = len,
        error => console.log(error));
  }

}
