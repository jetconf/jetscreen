import { TestBed, inject } from '@angular/core/testing';

import { RestconfService } from './restconf.service';

describe('RestconfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestconfService]
    });
  });

  it('should be created', inject([RestconfService], (service: RestconfService) => {
    expect(service).toBeTruthy();
  }));
});
