import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { InstanceValue, TerminalObject, ObjectValue, StructuredValue } from './instance-data';
import { SchemaDigest } from './schema-digest';

function topMember(rid: string[]): string {
  if (rid.length === 0) {
    return 'ietf-restconf:data';
  }
  let last = rid[rid.length - 1];
  let ieq = last.indexOf('=');
  if (ieq > 0) {
    last = last.slice(0, ieq);
  }
  if (last.indexOf(':') > 0) {
    return last;
  }
  let nid: string;
  for (let i = rid.length - 2; i >= 0; i--) {
    ieq = rid[i].indexOf('=');
    nid = ieq > 0 ? rid[i].slice(0, ieq) : rid[i];
    const icol = nid.indexOf(':');
    if (icol > 0) {
      return nid.slice(0, icol + 1) + last;
    }
  }
}

@Injectable()
export class RestconfService {
  jetconfUrl: string;

  constructor(private http: Http) {}

  private get apiUrl() { return this.jetconfUrl + '/restconf/'; }

  private handleError(error: Response | any) {
    return Observable.throw(error.json()['ietf-restconf:errors']['error'][0]);
  }

  reset(): Observable<boolean> {
    return this.http.post(this.apiUrl + 'operations/jetconf:conf-reset', {})
      .map(response => true)
      .catch(this.handleError);
    }

  commit(): Observable<boolean> {
    return this.http.post(this.apiUrl + 'operations/jetconf:conf-commit', {})
      .map(response => true)
      .catch(this.handleError);
  }

  put(rid: string[], value: InstanceValue): Observable<boolean> {
    const url = `${this.apiUrl}data/${rid.join('/')}`;
    const body: ObjectValue = {};
    body[topMember(rid)] = value;
    return this.http.put(url, JSON.stringify(body))
      .map(response => true)
      .catch(this.handleError);
  }

  getSchemaDigest(): Observable<SchemaDigest> {
    const url = this.apiUrl + 'operations/jetconf:get-schema-digest';
    return this.http.post(url, {})
      .map(response => response.json() as SchemaDigest)
      .catch(this.handleError);
  }

  getContent(rid: string[], depth?: number): Observable<StructuredValue> {
    let url: string = this.apiUrl + 'data';
    if (rid.length > 0) {
      url += '/' + rid.join('/');
    }
    if (depth !== undefined) {
      url += `?depth=${depth}`;
    }
    console.log('DATA:', url);
    return this.http.get(url)
      .map(response => response.json()[topMember(rid)] as StructuredValue)
      .catch(this.handleError);
  }

  getAllKeys(rid: string[], keys: string[]): Observable<TerminalObject[]> {
    function xKeys(obj: ObjectValue): ObjectValue {
      const res: ObjectValue = {};
      for (const m in obj) {
        if (keys.indexOf(m) >= 0) {
          res[m] = obj[m];
        }
      }
      return res;
    }
    const url = `${this.apiUrl}data/${rid.join('/')}?depth=2`;
    console.log('KEYS:', url);
    return this.http.get(url)
      .map(response =>
        (<ObjectValue[]>response.json()[topMember(rid)]).map(xKeys))
      .catch(this.handleError);
  }

  getLength(rid: string[]): Observable<number> {
    const url = this.apiUrl + 'operations/jetconf:get-list-length';
    const arg = { 'jetconf:input': { 'url': '/' + rid.join('/')}};
    console.log('LENGTH:', arg);
    return this.http.post(url, arg)
      .map(response => response.json()['jetconf:list-length'] as number)
      .catch(this.handleError);
  }

}
