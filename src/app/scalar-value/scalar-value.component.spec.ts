import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScalarValueComponent } from './scalar-value.component';

describe('ScalarValueComponent', () => {
  let component: ScalarValueComponent;
  let fixture: ComponentFixture<ScalarValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScalarValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScalarValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
