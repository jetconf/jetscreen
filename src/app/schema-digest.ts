type IntBaseType = 'int8' | 'int16' | 'int32' | 'int64' |
    'uint8' | 'uint16' | 'uint32' | 'uint64';

type OtherBaseType = 'boolean' | 'empty' | 'union';

type RangeSpec = [[number, number]];

interface DataType {
  derived?: string;
}

interface OtherType extends DataType {
  base: OtherBaseType;
}

interface IntegralType extends DataType {
  base: IntBaseType;
  range?: RangeSpec;
}

interface Decimal64Type extends DataType {
  base: 'decimal64';
  fraction_digits: number;
  range?: RangeSpec;
}

interface BinaryType extends DataType {
  base: 'binary';
  length?: RangeSpec;
}

interface StringType extends DataType {
  base: 'string';
  length?: RangeSpec;
  patterns?: string[];
  neg_patterns?: string[];
}

interface EnumerationType extends DataType {
  base: 'enumeration';
  enums?: string[];
}

interface BitsType extends DataType {
  base: 'bits';
  bits: string[];
}

interface LeafrefType extends DataType {
  base: 'leafref';
  ref_type: DataType;
}

interface IdentityrefType extends DataType {
  base: 'identityref';
  identities?: string[];
}

export type YangType = OtherType | IntegralType | Decimal64Type | BinaryType |
  StringType | EnumerationType | BitsType | LeafrefType | IdentityrefType;

type InternalKind = 'schematree' | 'container' | 'list' | 'input' | 'output' |
  'notification';

export type NodeKind = InternalKind | 'leaf' | 'leaf-list' | 'rpcaction';

interface NodeDigest {
  config?: boolean;
  description?: string;
}

export interface TerminalDigest extends NodeDigest {
  kind: 'leaf' | 'leaf-list';
  type: YangType;
  default?: string | number | boolean;
}

interface NontermDigest extends NodeDigest {
  children: {
    [ name: string ]: Digest;
  };
}

export interface SchemaDigest extends NontermDigest {
  kind: 'schematree';
}

interface ContainerDigest extends NontermDigest {
  kind: 'container';
  presence: boolean;
}

export interface ListDigest extends NontermDigest {
  kind: 'list';
  keys: Array<string>;
}

interface InputDigest extends NontermDigest {
  kind: 'input';
}

interface OutputDigest extends NontermDigest {
  kind: 'output';
}

interface RpcActionDigest extends NodeDigest {
  kind: 'rpcaction';
  input?: InputDigest;
  output?: OutputDigest;
}

interface NotificationDigest extends NontermDigest {
  kind: 'notification';
}

export type InternalDigest = SchemaDigest | ContainerDigest | ListDigest |
  InputDigest | OutputDigest | NotificationDigest;

export type Digest = TerminalDigest | InternalDigest | RpcActionDigest;

export enum ContentType {
  config = 1,
  nonconfig,
  all
}
