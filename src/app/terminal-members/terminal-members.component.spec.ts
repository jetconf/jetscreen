import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalMembersComponent } from './terminal-members.component';

describe('TerminalMembersComponent', () => {
  let component: TerminalMembersComponent;
  let fixture: ComponentFixture<TerminalMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminalMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminalMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
