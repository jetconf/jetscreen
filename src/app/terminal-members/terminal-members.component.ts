import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators, ValidatorFn } from '@angular/forms';

import { RestconfService } from '../restconf.service';
import { TreeNode } from '../tree-node';
import { ScalarValue, TerminalValue } from '../instance-data';
import { InternalDigest, ListDigest, TerminalDigest, YangType } from '../schema-digest';

@Component({
  selector: 'app-terminal-members',
  templateUrl: './terminal-members.component.html',
  styleUrls: ['./terminal-members.component.scss']
})
export class TerminalMembersComponent implements OnChanges {
  config = true;
  nonconfig = true;
  newMember: string = null;
  form: FormGroup;
  errors: string[] = [];

  constructor(private restconfService: RestconfService) { }

  @Input()
  node: TreeNode;
  @Input()
  entry: number;

  ngOnChanges(changes: SimpleChanges) {
    this.makeForm();
  }

  get members(): string[] {
    const keys: string[] = [];
    const other: string[] = [];
    for (const m of Object.keys(this.form.value)) {
      (this.isKey(m) ? keys : other).push(m);
    }
    return keys.concat(other.sort());
  }

  memberDigest(m: string): TerminalDigest {
    return this.node.digest.children[m] as TerminalDigest;
  }

  isKey(m: string): boolean {
    const dig: InternalDigest = this.node.digest;
    return dig.kind === 'list' && (<ListDigest>dig).keys.indexOf(m) >= 0;
  }

  isConfig(m: string): boolean {
    return this.node.config && this.memberDigest(m).config !== false;
  }

  isEditable(m: string): boolean {
    return this.isConfig(m) && !this.isKey(m);
  }

  validators(m: string): ValidatorFn[] {
    if (!this.isEditable(m)) {
      return;
    }
    const res: ValidatorFn[] = [];
    const td: YangType = this.memberDigest(m).type;
    switch (td.base) {
      case 'decimal64':
      case 'int8':
      case 'int16':
      case 'int32':
      case 'int64':
      case 'uint8':
      case 'uint16':
      case 'uint32':
      case 'uint64':
        break;
      case 'string':
        if (td.patterns) {
          res.concat(td.patterns.map(p => Validators.pattern(p)));
        }
        if (td.length) {
          const minl = td.length[0][0];
          if (minl !== null) {
            if (minl > 0) {
              res.push(Validators.required);
            }
            if (minl > 1) {
              res.push(Validators.minLength(minl));
            }
          }
          const maxl = td.length[td.length.length - 1][1];
          if (maxl !== null) {
            res.push(Validators.maxLength(maxl));
          }
        }
        break;
      case 'binary':
        break;
    }
    return res;
  }

  type(m: string): string {
    const mdig = this.memberDigest(m);
    return mdig.type.derived ? mdig.type.derived : mdig.type.base;
  }

  cssClasses(m: string): string[] {
    const res: string[] = [];
    if (this.isKey(m)) {
      res.push('key');
    }
    if (this.errors[m] !== undefined || !this.form.controls[m].valid) {
      res.push('invalid');
    }
    return res;
  }

  private makeForm(): void {
    this.form = new FormGroup({});
    for (const m in this.node.terminals) {
      if (this.node.terminals.hasOwnProperty(m)) {
        this.createMember(m, this.node.terminals[m]);
      }
    }
  }

  creatable(): string[] {
    const exist: string[] = Object.keys(this.form.value);
    const res: string[] = [];
    for (const m in this.node.digest.children) {
      if (this.node.digest.children.hasOwnProperty(m)) {
        const mdig = this.memberDigest(m);
        if (mdig.config !== false && exist.indexOf(m) < 0 &&
          (mdig.kind === 'leaf' || mdig.kind === 'leaf-list')) {
            res.push(m);
          }
      }
    }
    return res;
  }

  addNewMember(): void {
    this.createMember(this.newMember);
    this.newMember = null;
  }

  private createMember(m: string, value?: TerminalValue): void {
    const mdig = this.memberDigest(m);
    value = value === undefined ? mdig.default : value;
    let ctrl: AbstractControl;
    if (mdig.kind === 'leaf') {
      ctrl = new FormControl(value === undefined ? null : value);
    } else {
      ctrl = new FormArray([]);
      if (value !== undefined) {
        for (const v of <ScalarValue[]>value) {
          (<FormArray>ctrl).push(new FormControl(v));
        }
      }
    }
    this.form.addControl(m, ctrl);
  }

  sendData(): void {
    this.errors = [];
    const fval = this.form.value;
    for (const m in fval) {
      if (this.form.controls[m].pristine) {
        continue;
      }
      this.restconfService.put(this.node.resourceID().concat(m), fval[m])
        .subscribe(success => console.log('PUT:', `${this.node.jsonPointer()}/${m}`),
        error => this.errors[m] = error['error-message']);
    }
  }

  discardChanges(): void {
    this.makeForm();
    this.errors = [];
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

}
